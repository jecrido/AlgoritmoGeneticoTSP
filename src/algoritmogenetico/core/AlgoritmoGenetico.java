package algoritmogenetico.core;

import algoritmogenetico.operadores.Cruce;
import algoritmogenetico.operadores.Mutacion;
import algoritmogenetico.operadores.Seleccion;
import algoritmogenetico.procesos.Adaptacion;
import algoritmogenetico.procesos.Convergencia;
import algoritmogenetico.procesos.Poblacion;
import algoritmogenetico.procesos.Rutas;
import static algoritmogenetico.tsp.AlgoritmoGeneticoRun.runMethod;
import java.util.Scanner;

public class AlgoritmoGenetico {

    private int numeroNodos;
    private int tamañoPoblacion;
    private int poblacion[][];
    
    Rutas red;
    Poblacion individuos;
    Adaptacion funcion;
    Seleccion parejas;
    Cruce cruce;
    Mutacion mutar;
    Convergencia convergio;
    int minMax;
    public void generarRutas() {
        red = new Rutas();
        Scanner sc = new Scanner(System.in);
        System.out.println("Manimizar/Maximizar (1/0) : ");
        minMax = sc.nextInt();
        System.out.println("OPCIONES PARA GENERAR RED : ");
        System.out.println("1. Desde archivo Excel");
        System.out.println("2. Red de prueba por defecto");
        System.out.println("3. Generar aleatoriamente");
        System.out.print("Escoger opción : ");
        
        int opcion = sc.nextInt();
        switch(opcion){
            case 1:
                System.out.println("\nSELECCIONAR ARCCHIVO");
                red.generarDesdeExcel();
                break;
            case 2:
                System.out.println("\nRED DE PRUEBA POR DEFECTO");
                red.generarRutasPrueba();
                break;
            case 3:
                System.out.println("\nRED DE PRUEBA GENERADO ALEATORIAMENTE \n");
                System.out.print("Numero de nodos : ");
                int nNodos = sc.nextInt();
                System.out.print("Minimo valor de peso (distancia, tiempo, etc) > 0 : ");
                int minPeso = sc.nextInt();
                System.out.print("Maximo valor de peso (distancia, tiempo, etc) > 0 : ");
                int maxPeso = sc.nextInt();
                System.out.print("El valor de peso es tipo entero? (si:1, no:0) : ");
                int entero = sc.nextInt();
                boolean esEntero = false;
                if (entero == 1) esEntero = true;
                red.generarRutasAleatorias(nNodos, minPeso, maxPeso, esEntero);
                break;
        }
//        red.generarRutasPrueba();
//        red.generarDesdeExcel();
//        red.generarRutasAleatorias(50, 1, 1000, true);
        numeroNodos = red.getNumeroNodos();
        tamañoPoblacion = numeroNodos*2;
//        System.out.println("N: "+numeroNodos);
        System.out.println(red.imprimirRutas());
    }

    public void poblacionInicial() {
        individuos = new Poblacion(tamañoPoblacion, numeroNodos);
        individuos.generarPoblacionInicial();
        poblacion=individuos.getPoblacion_inicial();
        valorPoblacion();
        if (funcion.poblacionInvalida()) {
            poblacionInicial();
        }
    }
    

    public void valorPoblacion() {
        funcion = new Adaptacion(tamañoPoblacion, numeroNodos, poblacion);
        funcion.computar(red.getRutas());
        if (minMax == 1) {
            funcion.minimizar();
        } else{
            funcion.maximizar();
        }
        //System.out.println(funcion.imprimirPoblacionValor());
    }
    
    public void operadorSeleccion(){
        parejas = new Seleccion(tamañoPoblacion, numeroNodos, poblacion, funcion.getValor(), funcion.getCriterio(), funcion.getMejorAdaptado(), funcion.getValido());
        parejas.colaReproduccion();
//        System.out.println(parejas.imprimirParejas());
    }
    
    public void operadorCruce(){
        cruce = new Cruce(tamañoPoblacion, numeroNodos, 0.5);
//        cruce.noAleatorio(10);
        cruce.cruzamiento(parejas.getPadreA(), parejas.getPadreB());
//        System.out.println(cruce.getImpriTablaCor());
//        System.out.println(cruce.imprimirHijosCruzados());
    }
    
    public void operadorMutacion(){
        mutar = new Mutacion(tamañoPoblacion, numeroNodos,0.95, cruce.getHijos());
        mutar.mutacion();
//        System.out.println(mutar.imprimirTablaMutacion());
    }
    
    public void reemplazarPoblacion(){
       
        poblacion=mutar.getHijos();
                
    }
    
    public boolean convergencia(){
        convergio = new Convergencia(funcion.getAdaptacionMedia(), funcion.getMejorAdaptado(), funcion.getCriterio());
        return convergio.convergio();
    }
    
    public void resultString(){
        System.out.println(funcion.imprimirPoblacionValor());
    }

    public Adaptacion getFuncion() {
        return funcion;
    }
    
}
