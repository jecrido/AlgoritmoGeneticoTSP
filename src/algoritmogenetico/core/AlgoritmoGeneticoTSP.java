package algoritmogenetico.core;

import algoritmogenetico.core.AlgoritmoGenetico;
import java.util.Arrays;

public class AlgoritmoGeneticoTSP {
        AlgoritmoGenetico TSP = new AlgoritmoGenetico();
        int generacion;
        double adaptacionMedia = 0;
        double mejorAdaptado = 0;
        
        public void AGCore(){
            generacion = 0;
                do {
                    //System.out.println("Generación: "+generacion);
                    TSP.operadorSeleccion();
                    TSP.operadorCruce();
                    TSP.operadorMutacion();
                    TSP.reemplazarPoblacion();
                    TSP.valorPoblacion();
                    generacion++;
                } while (TSP.convergencia());
                System.out.println("Última generación : \n");
                TSP.resultString();
        }
        
        public void AGRunOne(){
            TSP.generarRutas();
            System.out.println("Poblacion Inicial : \n");
            TSP.poblacionInicial();
            TSP.resultString();
            adaptacionMedia = TSP.getFuncion().getAdaptacionMedia();
            mejorAdaptado = TSP.getFuncion().getMejorAdaptado();
            AGCore();
            System.out.println("Número de generaciones : " + generacion + "\n\n");
        }
        
        public void AGRunMultiple(int repPoblacionIncialIgual, int numPoblacionIncialDiferente){
            generacion = 0;
            TSP.generarRutas();
        //Contador y bucle de corridas
            int corrida1 = repPoblacionIncialIgual;
            int corrida2 = numPoblacionIncialDiferente;
            int contador1 = 0, contador2 = 0;
            int contador = 0;
            double resultados[] = new double[corrida1 * corrida2];
            while (contador1 < corrida1) {
                TSP.poblacionInicial();
                TSP.resultString();
                adaptacionMedia = TSP.getFuncion().getAdaptacionMedia();
                mejorAdaptado = TSP.getFuncion().getMejorAdaptado();
                contador2 = 0;
                while (contador2 < corrida2) {

                    AGCore();
                    System.out.println("..........................RESULTADO.[" + contador + "].......................\n");
                    System.out.println("GENERACION: " + generacion);
                    System.out.println("CRITERIO \t " + "PRIMERA G \t " + "ULTIMA G \t " + "DIFERENCIA\n");
                    System.out.println("Adap. Media: \t " + adaptacionMedia + " \t " + TSP.getFuncion().getAdaptacionMedia() + " \t " + (adaptacionMedia - TSP.getFuncion().getAdaptacionMedia()) + "\n");
                    System.out.println("Mej. Adaptado: \t " + mejorAdaptado + " \t " + TSP.getFuncion().getMejorAdaptado() + " \t " + (mejorAdaptado - TSP.getFuncion().getMejorAdaptado()) + "\n");
                    resultados[contador] = TSP.getFuncion().getMejorAdaptado();
                    contador++;
                    contador2++;
                }
                contador1++;
            }
            Arrays.sort(resultados);
            System.out.println("*************************************************************\n");
            System.out.println("MEJOR RESULTADO: \t" + resultados[0]);
            System.out.println("*************************************************************\n");
            }

}
