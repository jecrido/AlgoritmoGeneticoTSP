package algoritmogenetico.operadores;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author dosan
 */
public class Cruce {

    private int tablaCorrespondencia[][];
    private int tamaño;
    private int numeroCiudades;
    private double probCruce;
    private double reducc;
    private int hijos[][];
    private double punto_cruce[][];
    private String impriTablaCor="Tabla de correspondencia: \n";
    private boolean aleatorio = true;
    private int cantidadGenesCruzar;
    
    public Cruce(int tamaño, int numeroCiudades, double probCruce) {
        this.tamaño = tamaño;
        this.numeroCiudades = numeroCiudades;
        this.probCruce = probCruce;
        reducc = 0.7;
        hijos = new int[this.tamaño][this.numeroCiudades];
    }

    public int[][] getHijos() {
        return hijos;
    }

    public boolean isAleatorio() {
        return aleatorio;
    }

    public void setAleatorio(boolean aleatorio) {
        this.aleatorio = aleatorio;
    }

    public int getCantidadGenesCruzar() {
        return cantidadGenesCruzar;
    }

    public void setCantidadGenesCruzar(int cantidadGenesCruzar) {
        this.cantidadGenesCruzar = cantidadGenesCruzar;
    }

    public String getImpriTablaCor() {
        return impriTablaCor;
    }
    
    public void noAleatorio(int cantidadGenesCruzar){
        this.aleatorio = false;
        this.cantidadGenesCruzar = cantidadGenesCruzar;
    }
    
    public void probabilidadCruce() {
        float x;
        int minimo=(int)0.3*numeroCiudades;
        int maximo=(int)reducc * numeroCiudades;
        punto_cruce = new double[tamaño / 2][2];
        for (int i = 0; i < tamaño / 2; i++) {

            x = (float) (Math.random() * 10 + 1) / 10;

            punto_cruce[i][0] = x;
            if (punto_cruce[i][0] >= probCruce) {
                
                if(aleatorio){
               // punto_cruce[i][1] = (int) ((Math.random() * (minimo) - (maximo)) + (maximo));//Primero
                punto_cruce[i][1] = (int) (Math.random() * ((maximo) - (minimo)) + minimo);
                }else{
                    punto_cruce[i][1] = cantidadGenesCruzar;
                }
                
            } else {
                punto_cruce[i][1] = 0;

            }

        }
    }

    public void seleccionarPosiciones(int nroPareja) {
        tablaCorrespondencia = new int[(int) punto_cruce[nroPareja][1]][5];
        int k;
        int[] numeros = new int[numeroCiudades];
        Random rnd = new Random();
        int res;
        int[] ordenado = new int[(int) punto_cruce[nroPareja][1]];

        k = numeroCiudades;
        for (int i = 0; i < numeroCiudades; i++) {
            numeros[i] = i;
        }
        for (int i = 0; i < (int) punto_cruce[nroPareja][1]; i++) {
            res = rnd.nextInt(k);
            ordenado[i] = numeros[res];
            tablaCorrespondencia[i][0] = numeros[res];
            numeros[res] = numeros[k - 1];
            k--;

        }
        Arrays.sort(ordenado);
        for (int i = 0; i < (int) punto_cruce[nroPareja][1]; i++) {
            tablaCorrespondencia[i][0] = ordenado[i];
        }
    }

    public void cruzamiento(int padreA[][], int padreB[][]) {
        probabilidadCruce();
        int nroPareja = 0;
        for (int nroHijo = 0; nroHijo < tamaño; nroHijo = nroHijo + 2) {
            if (punto_cruce[nroPareja][0] < probCruce) {
                for (int j = 0; j < numeroCiudades; j++) {
                    hijos[nroHijo][j] = padreA[nroPareja][j];
                    hijos[nroHijo + 1][j] = padreB[nroPareja][j];
                }
            } else {
                impriTablaCor+="Pareja [" + nroPareja + "]: \n";
                seleccionarPosiciones(nroPareja);
                for (int j = 0; j < punto_cruce[nroPareja][1]; j++) {
                    tablaCorrespondencia[j][1] = padreA[nroPareja][tablaCorrespondencia[j][0]];
                    tablaCorrespondencia[j][2] = padreB[nroPareja][tablaCorrespondencia[j][0]];
                    tablaCorrespondencia[j][3] = 0;
                    tablaCorrespondencia[j][4] = 0;
                }
                for (int j = 0; j < punto_cruce[nroPareja][1]; j++) {
                    for (int k = 0; k < 5; k++) {
                        impriTablaCor+=tablaCorrespondencia[j][k] + "\t";
                    }
                    impriTablaCor+="\n";
                }
                intercambioGenes(nroHijo, nroPareja, padreA, 1, punto_cruce);
                intercambioGenes((nroHijo + 1), nroPareja, padreB, 2, punto_cruce);
            }
            nroPareja++;
        }
    }

    public void intercambioGenes(int nroHijo, int nroPareja, int padre[][], int b, double cr[][]) {
        int a = 0, c = 0, d = 0, e = 0;
        if (b == 1) {
            a = 2;
            c = 1;
            d = 4;
            e = 3;
        }
        if (b == 2) {
            a = 1;
            c = 2;
            d = 3;
            e = 4;
        }
        int bandera, z = 0;
        int pos = 0;
        for (int j = 0; j < numeroCiudades; j++) {//RECORRER TODA LA GIRA
            if (buscar(j, cr, nroPareja, 0) == -1) {

                if (buscar(padre[nroPareja][j], cr, nroPareja, a) == -1) {
                    hijos[nroHijo][j] = padre[nroPareja][j];
                } else {

                    pos = buscar(padre[nroPareja][j], cr, nroPareja, a);
                    if (tablaCorrespondencia[pos][3] + tablaCorrespondencia[pos][4] == 0) {
                        hijos[nroHijo][j] = tablaCorrespondencia[pos][c];
                        tablaCorrespondencia[pos][3] = 1;
                        tablaCorrespondencia[pos][4] = 1;

                        if (buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a) != -1) {
                            if (tablaCorrespondencia[buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a)][e] != 1) {
                                tablaCorrespondencia[buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a)][d] = 1;
                            }
                        }
                        if (buscar(tablaCorrespondencia[pos][a], cr, nroPareja, c) != -1) {
                            if (tablaCorrespondencia[buscar(tablaCorrespondencia[pos][a], cr, nroPareja, c)][d] != 1) {
                                tablaCorrespondencia[buscar(tablaCorrespondencia[pos][a], cr, nroPareja, c)][e] = 1;
                            }
                        }

                    } else {
                        if (tablaCorrespondencia[pos][3] + tablaCorrespondencia[pos][4] == 2) {
                            hijos[nroHijo][j] = tablaCorrespondencia[pos][c];
                        } else {
                            if (tablaCorrespondencia[pos][3] + tablaCorrespondencia[pos][4] == 1) {
                                hijos[nroHijo][j] = padre[nroPareja][j];

                            }
                        }
                    }
                }
            } else {  //SI FUE SELECCIONADO EN MATRIZ
                pos = buscar(padre[nroPareja][j], cr, nroPareja, c);
                if (tablaCorrespondencia[pos][3] + tablaCorrespondencia[pos][4] == 0) {//correspondencia 0
                    hijos[nroHijo][j] = tablaCorrespondencia[pos][a];//intercambiar valor
                    tablaCorrespondencia[pos][3] = 1;//marcar corresponencia
                    tablaCorrespondencia[pos][4] = 1;//marcar correspondencia
                    if (buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a) != -1) {
                        if (tablaCorrespondencia[buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a)][e] != 1) {
                            tablaCorrespondencia[buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a)][d] = 1;
                        }
                    }//buscar valor opuesto en actual si se encuentra marcar correspondencia
                    if (buscar(tablaCorrespondencia[pos][a], cr, nroPareja, c) != -1) {
                        if (tablaCorrespondencia[buscar(tablaCorrespondencia[pos][a], cr, nroPareja, c)][d] != 1) {
                            tablaCorrespondencia[buscar(tablaCorrespondencia[pos][a], cr, nroPareja, c)][e] = 1;
                        }
                    }
                } else {//Correspondencia no es cero
                    if (tablaCorrespondencia[pos][3] + tablaCorrespondencia[pos][4] == 2) {//Correspondencia 2
                        hijos[nroHijo][j] = tablaCorrespondencia[pos][a];//intercambiar valor
                    } else {
                        if (tablaCorrespondencia[pos][3] + tablaCorrespondencia[pos][4] == 1) {//Correspondencia 1
                            if (buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a) != -1) {
                                int pos2 = buscar(tablaCorrespondencia[pos][c], cr, nroPareja, a);
                                if (tablaCorrespondencia[pos2][3] + tablaCorrespondencia[pos2][4] == 2) {
                                    hijos[nroHijo][j] = tablaCorrespondencia[pos2][c];
                                } else {
                                    if (tablaCorrespondencia[pos2][3] + tablaCorrespondencia[pos2][4] == 0) {
                                        hijos[nroHijo][j] = tablaCorrespondencia[pos2][c];
                                        tablaCorrespondencia[pos2][3] = 1;
                                        tablaCorrespondencia[pos2][4] = 1;
                                        if (buscar(tablaCorrespondencia[pos2][c], cr, nroPareja, a) != -1) {
                                            tablaCorrespondencia[buscar(tablaCorrespondencia[pos2][c], cr, nroPareja, a)][d] = 1;
                                        } else {
                                            hijos[nroHijo][j] = tablaCorrespondencia[pos2][c];
                                        }
                                    } else {
                                        if (tablaCorrespondencia[pos2][3] + tablaCorrespondencia[pos2][4] == 1) {
                                            hijos[nroHijo][j] = padre[nroPareja][j];
                                        }
                                    }

                                }

                            } else {//agregado
                                hijos[nroHijo][j] = padre[nroPareja][j];
                            }
                        }
                    }
                }
            }
        }
    }

    public int buscar(int padre, double cr[][], int p, int a) {
        int bandera = -1;
        int z = 0;
        while (z < (int) cr[p][1]) {
            if (padre == tablaCorrespondencia[z][a]) {
                bandera = z;
            }
            z++;
        }
        return bandera;
    }
    
    public String imprimirHijosCruzados(){
        String cadena = "Hijos cruzados: \n";
        for (int i = 0; i < tamaño; i++) {
            cadena += "[" + i + "]: \t";
            for (int j = 0; j < numeroCiudades; j++) {
                cadena += hijos[i][j] + "\t";
            }
           cadena += "\n";
        }
        return cadena;
    }
}
