/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmogenetico.operadores;

/**
 *
 * @author dosan
 */
public class Mutacion {

    private int tamaño;
    private int numeroCiudades;
    private int hijos[][];
    private double prob[][];
    private double probMuta;

    public Mutacion(int tamaño, int numeroCiudades, double probMuta, int hijos[][]) {
        this.tamaño = tamaño;
        this.numeroCiudades = numeroCiudades;
        this.probMuta = probMuta;
        this.hijos = hijos;
    }

    public int[][] getHijos() {
        return hijos;
    }

    public void mutacion() {
        probabilidadMutacion();
    //    mutacionInversionSimple();
        mutacionCambioDosPuntos();
    }

    public void probabilidadMutacion() {
        prob = new double[tamaño][3];
        int x = -1;
        for (int i = 0; i < tamaño; i++) {
            prob[i][0] = ((double) Math.random() * 10 + 1) / 10;
            if (prob[i][0] >= probMuta) {
                prob[i][1] = (int) (Math.random() * (numeroCiudades - 1));

                do {
                    prob[i][2] = (int) (Math.random() * (numeroCiudades - 1));
                } while (prob[i][2] == prob[i][1]);

            } else {
                prob[i][1] = 0;
                prob[i][2] = 0;

            }

        }
    }

    public void mutacionInversionSimple() {
        int pos;
        int cant;
        int aux1, aux2, aux3, aux4;
//        int minimo = (int) 0.05 * numeroCiudades;
        int minimo = (int) 0.05 * numeroCiudades;
        int maximo = (int) 0.2 * numeroCiudades - 1;
        for (int i = 0; i < tamaño; i++) {
            if (prob[i][0] >= probMuta) {
                pos = (int) (Math.random() * (numeroCiudades - 1));
                cant = (int) (Math.random() * ((maximo) - (minimo)) + minimo);
                if (pos < (numeroCiudades / 2)) {
                    aux1 = pos + (cant / 2);
                    aux2 = pos + cant - 1;
                    for (int j = pos; j < aux1; j++) {
                        aux3 = hijos[i][j];
                        aux4 = hijos[i][aux2];
                        hijos[i][aux2] = aux3;
                        hijos[i][j] = aux4;
                        aux2--;
                    }
                }else{
                    if (pos >= (numeroCiudades / 2)) {
                    aux1 = pos - (cant / 2);
                    aux2 = pos - cant + 1;
                    for (int j = pos; j > aux1; j--) {
                        aux3 = hijos[i][j];
                        aux4 = hijos[i][aux2];
                        hijos[i][aux2] = aux3;
                        hijos[i][j] = aux4;
                        aux2++;
                    }
                }
                }
            }
        }
    }

    public void mutacionCambioDosPuntos() {
        int a;
        int b;
        for (int i = 0; i < tamaño; i++) {
            a = hijos[i][(int) prob[i][1]];
            b = hijos[i][(int) prob[i][2]];
            hijos[i][(int) prob[i][1]] = b;
            hijos[i][(int) prob[i][2]] = a;
        }
    }

    public String imprimirTablaMutacion() {
        String cadena = "Tabla de mutacion: \n";
        for (int i = 0; i < tamaño; i++) {
            cadena += "Hijo [" + i + "]: \t";
            for (int j = 0; j < 3; j++) {
                cadena += prob[i][j] + "\t";
            }
            cadena += "\n";
        }
        return cadena;
    }
}
