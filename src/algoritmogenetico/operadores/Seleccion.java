/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmogenetico.operadores;

/**
 *
 * @author dosan
 */
public class Seleccion {

    private int penalidad; //penalidad aplicada a seleccion del mejor adaptado
    private int tamaño;
    private int numeroCiudades;
    private int padreA[][];
    private int padreB[][];
    private int poblacion_inicial[][];
    private int torneo[];
    private double valor[];
    private int valido[][];
    private int participantes;
    private int criterio;
    private double mejorAdaptado;
    private int posPadreA[];
    private int posPadreB[];

    public int[][] getPadreA() {
        return padreA;
    }

    public int[][] getPadreB() {
        return padreB;
    }

    public Seleccion() {
    }

    public Seleccion(int tamaño, int numeroCiudades, int[][] poblacion_inicial, double[] valor, int criterio, double mejorAdaptado, int valido[][]) {
        this.tamaño = tamaño;
        this.numeroCiudades = numeroCiudades;
        this.poblacion_inicial = poblacion_inicial;
        this.valor = valor;
        this.criterio = criterio;
        this.mejorAdaptado = mejorAdaptado;
        this.valido = valido;
    }

    public int torneoFase1() {
        if (tamaño <= 10) {participantes = (int) tamaño * 2 / 5;}
        if (tamaño > 10 && tamaño <= 20) {participantes = (int) tamaño / 5;}
        if (tamaño > 20) {participantes = (int) tamaño / 10;}
        torneo = new int[participantes];
        int i = 0, j;
        double mejor = 0;
        int finalist = 0;
        int aux = 0;
        while (aux == 0) {
            do { aux = (int) (Math.random() * (tamaño));
            } while (valido[aux][1] == 1);
        }
        torneo[0] = aux;
        for (i = 1; i < participantes; i++) {
            aux = 0;
            while (aux == 0) {
                do { aux = (int) (Math.random() * (tamaño));
                } while (valido[aux][1] == 1);
            }
            torneo[i] = aux;
            for (j = 0; j < i; j++) {
                if (torneo[i] == torneo[j]) {
                    i--;
                }
            }
        }
        mejor = valor[torneo[0]];
        finalist = torneo[0];
        for (int k = 1; k < participantes; k++) {
            if (criterio == 1) {
                if (valor[torneo[k]] > mejor) {//Maximizar
                    mejor = valor[torneo[k]];
                    finalist = torneo[k];
                }
            } else {
                if (valor[torneo[k]] < mejor) {//Minimizar
                    mejor = valor[torneo[k]];
                    finalist = torneo[k];
                }
            }
        }
        if (valor[finalist] == mejorAdaptado) {
            penalidad++;
        }
        return finalist;
    }

    public int torneoFase2() {
        int ganador;
                int criterio=0;
        if (tamaño <= 10) {criterio = (int) tamaño * 2 / 5;}
        if (tamaño > 10 && tamaño <= 20) {criterio = (int) tamaño / 5;}
        if (tamaño > 20) {criterio = (int) tamaño / 10;}
        if (penalidad == criterio) {
//        if (tamaño <= 10) {penalidad = (int) tamaño * 2 / 5;}
//        if (tamaño > 10 && tamaño <= 20) {penalidad = (int) tamaño / 5;}
//        if (tamaño > 20) {penalidad = (int) tamaño / 10;}
//        if (penalidad == tamaño * 5 / 10) {
            do {
                ganador = torneoFase1();
            } while (valor[torneoFase1()] == mejorAdaptado);

        } else {
            ganador = torneoFase1();
            if (valor[torneoFase1()] == mejorAdaptado) {
                penalidad++;
            }
        }
        return ganador;
    }

    public void colaReproduccion() {
        padreA = new int[tamaño / 2][numeroCiudades];
        padreB = new int[tamaño / 2][numeroCiudades];
        posPadreA = new int[tamaño / 2];
        posPadreB = new int[tamaño / 2];

        int p;
        for (int i = 0; i < tamaño / 2; i++) {
            p = 0;
            while (p == 0) {

                posPadreA[i] = torneoFase2();
                posPadreB[i] = torneoFase2();

                if (posPadreA[i] != posPadreB[i]) {

                    for (int j = 0; j < numeroCiudades; j++) {

                        padreA[i][j] = poblacion_inicial[posPadreA[i]][j];
                        padreB[i][j] = poblacion_inicial[posPadreB[i]][j];

                    }
                    p = 1;
                } else {
                    p = 0;
                }
            }
        }
    }

    public String imprimirParejas() {
        String cadPaA;
        String cadPaB;
        String cadena = "Parejas seleccionadas: \n";
        String cadena2 = "";
        for (int i = 0; i < tamaño / 2; i++) {
            cadPaA = "";
            cadPaB = "";
            for (int j = 0; j < numeroCiudades; j++) {
                cadPaA += padreA[i][j] + "\t";
                cadPaB += padreB[i][j] + "\t";
            }
            cadena2 += "Pareja [" + i + "]: \t" + "\n" + "Padre[" + posPadreA[i] + "]: \t" + cadPaA + "\n" + "Padre[" + posPadreB[i] + "]: \t" + cadPaB + "\n";
        }
        return cadena + cadena2;
    }

}
