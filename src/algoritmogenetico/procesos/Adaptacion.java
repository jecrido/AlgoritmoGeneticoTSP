/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmogenetico.procesos;

/**
 *
 * @author dosan
 */
public class Adaptacion {

    private double valor[];
    private double adaptacionMedia;
    private double mejorAdaptado;
    private int valido[][];
    private int criterio;
    private int tamaño;
    private int numeroCiudades;
    private int poblacion_inicial[][];

    public double[] getValor() {
        return valor;
    }

    public int[][] getValido() {
        return valido;
    }

    public double getAdaptacionMedia() {
        return adaptacionMedia;
    }

    public double getMejorAdaptado() {
        return mejorAdaptado;
    }

    public Adaptacion(int tamaño, int numeroCiudades, int poblacion_inicial[][]) {
        this.tamaño = tamaño;
        this.numeroCiudades = numeroCiudades;
        this.poblacion_inicial = poblacion_inicial;
    }

    public int getCriterio() {
        return criterio;
    }

    public Adaptacion() {

    }

    public void computar(double mapa[][]) {
        valor = new double[tamaño];
        valido = new int[tamaño][2];
        int invalido = 0;
        int j = 0;
        for (int i = 0; i < tamaño; i++) {
            valor[i] = 0;
        }
        for (int i = 0; i < tamaño; i++) {
            for (j = 0; j < numeroCiudades - 1; j++) {
                if (mapa[poblacion_inicial[i][j] - 1][poblacion_inicial[i][j + 1] - 1] == -1) {
                    valido[i][1] = 1;
                }
                valor[i] += mapa[poblacion_inicial[i][j] - 1][poblacion_inicial[i][j + 1] - 1];
            }

            if (mapa[poblacion_inicial[i][j] - 1][poblacion_inicial[i][0] - 1] == -1) {
                valido[i][1] = 1;
            }
            valor[i] += mapa[poblacion_inicial[i][j] - 1][poblacion_inicial[i][0] - 1];
        }
        int prom = 0;
        int sum = 0;
        for (int i = 0; i < tamaño; i++) {
            prom += valor[i];
            for (int k = 0; k < numeroCiudades; k++) {
                sum += poblacion_inicial[i][k];
            }
            if (sum == ((numeroCiudades * (numeroCiudades - 1) / 2) + numeroCiudades)) {
                valido[i][0] = sum;
            }
            sum = 0;
        }
        adaptacionMedia = (double) prom / tamaño;
    }

    public void maximizar() {
        criterio = 1;
        int aux;
        do {
            aux = (int) (Math.random() * (tamaño));
        } while (valido[aux][1] == 1);

        double mayor = valor[aux];

        for (int k = 0; k < tamaño; k++) {

            if (valor[k] > mayor && valido[k][1] == 0) {
                mayor = valor[k];
            }

        }
        mejorAdaptado = (double) mayor;
    }

    public void minimizar() {
        criterio = 0;
        int aux;
        do {
            aux = (int) (Math.random() * (tamaño));
        } while (valido[aux][1] == 1);
        double menor = valor[aux];

        for (int k = 0; k < tamaño; k++) {

            if (valor[k] < menor && valido[k][1] == 0) {
                menor = valor[k];
            }
        }
        mejorAdaptado = (double) menor;
    }

    public String imprimirValor() {
        String cadena = "Valor de la poblacion: \n";
        for (int i = 0; i < tamaño; i++) {
            cadena += "[" + i + "] \t" + valor[i] + "\n";
        }
        return cadena;
    }

    public boolean poblacionInvalida() {
        int suma = 0;
        boolean respuesta = false;
        for (int i = 0; i < tamaño; i++) {
            suma += valido[i][1];
        }
        if (suma > tamaño / 2) {
            respuesta = true;
        }
        return respuesta;
    }

    public String imprimirPoblacionValor() {
        String cadena = "Poblacion: \n";
        for (int i = 0; i < tamaño; i++) {
            cadena += "[" + i + "]: \t";
            for (int j = 0; j < numeroCiudades; j++) {
                cadena += poblacion_inicial[i][j] + "\t";
            }
            cadena += ": \t" + valor[i] + ", valido: " + valido[i][0] + ":" + valido[i][1] + "\n";
        }
        cadena += "Adaptación media: " + getAdaptacionMedia() + "\n";
        cadena += "Mejor adaptado: " + getMejorAdaptado() + "\n";
        return cadena;
    }
}
