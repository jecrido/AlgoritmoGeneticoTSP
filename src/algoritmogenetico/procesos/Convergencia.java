/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmogenetico.procesos;

/**
 *
 * @author dosan
 */
public class Convergencia {
    private double adaptacionMedia;
    private double mejorAdaptado;
    private int criterio;

    public Convergencia(double adaptacionMedia, double mejorAdaptado, int criterio) {
        this.adaptacionMedia = adaptacionMedia;
        this.mejorAdaptado = mejorAdaptado;
        this.criterio = criterio;
    }
    
    public boolean convergio(){
        boolean si = false;
        if (criterio == 0) {
            if (mejorAdaptado < (adaptacionMedia*0.99)){
                si = true;
            }
        }
        else if (criterio == 1) {
            if (adaptacionMedia < mejorAdaptado*0.99){
                si =true;
            }
        }
        return si;
    }
}
