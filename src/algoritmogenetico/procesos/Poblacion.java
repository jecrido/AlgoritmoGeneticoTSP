package algoritmogenetico.procesos;

import java.util.Random;

/**
 *
 * @author dosan
 */
public class Poblacion {

    private int tamaño;
    private int numeroCiudades;
    private int poblacion_inicial[][];

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public int getNumeroCiudades() {
        return numeroCiudades;
    }

    public void setNumeroCiudades(int numeroCiudades) {
        this.numeroCiudades = numeroCiudades;
    }

    public int[][] getPoblacion_inicial() {
        return poblacion_inicial;
    }

    public void setPoblacion_inicial(int[][] poblacion_inicial) {
        this.poblacion_inicial = poblacion_inicial;
    }

    public Poblacion() {
    }

    public Poblacion(int tamaño, int numeroCiudades) {
        this.tamaño = tamaño;
        this.numeroCiudades = numeroCiudades;
        poblacion_inicial = new int[this.tamaño][this.numeroCiudades];
    }

    public void generarPoblacionInicial() {
        int k;
        int[] numeros = new int[numeroCiudades];
        Random rnd = new Random();
        int res;

        for (int j = 0; j < tamaño; j++) {
            k = numeroCiudades;
            for (int i = 0; i < numeroCiudades; i++) {
                numeros[i] = i + 1;
            }
            for (int i = 0; i < numeroCiudades; i++) {
                res = rnd.nextInt(k);
                poblacion_inicial[j][i] = numeros[res];
                numeros[res] = numeros[k - 1];
                k--;

            }
        }
    }

    public String imprimirPoblacion() {
        String cadena = "Poblacion: \n";
        for (int i = 0; i < tamaño; i++) {
            cadena += "[" + i + "]: \t";
            for (int j = 0; j < numeroCiudades; j++) {
                cadena += poblacion_inicial[i][j] + "\t";
            }
           cadena += "\n";
        }
        return cadena;
    }

}
