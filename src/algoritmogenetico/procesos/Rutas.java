package algoritmogenetico.procesos;

import java.text.DecimalFormat;

/**
 *
 * @author dosan
 */
public class Rutas {

    private int numeroNodos = 5;
    private double rutas[][];

    public Rutas() {

    }

    public Rutas(int numeroCiudades) {
        this.numeroNodos = numeroCiudades;
    }

    public int getNumeroNodos() {
        return numeroNodos;
    }

    public void setNumeroNodos(int numeroNodos) {
        this.numeroNodos = numeroNodos;
    }

    public double[][] getRutas() {
        return rutas;
    }

    public void setRutas(double[][] rutas) {
        this.rutas = rutas;
    }

    public void generarRutasPrueba() {
        rutas = new double[numeroNodos][numeroNodos];

        rutas[0][0] = 0;
        rutas[0][1] = 24;
        rutas[0][2] = 10;
        rutas[0][3] = 3;
        rutas[0][4] = 12;

        rutas[1][0] = 12;
        rutas[1][1] = 0;
        rutas[1][2] = 21;
        rutas[1][3] = 7;
        rutas[1][4] = 4;

        rutas[2][0] = 11;
        rutas[2][1] = 21;
        rutas[2][2] = 0;
        rutas[2][3] = 14;
        rutas[2][4] = 5;

        rutas[3][0] = 3;
        rutas[3][1] = 12;
        rutas[3][2] = 18;
        rutas[3][3] = 0;
        rutas[3][4] = 3;

        rutas[4][0] = 18;
        rutas[4][1] = 21;
        rutas[4][2] = 5;
        rutas[4][3] = 12;
        rutas[4][4] = 0;
    }

    public void generarRutasAleatorias(int numeroCiudades, int minimoValor, int maximoValor, boolean entero) {
        this.numeroNodos=numeroCiudades;
        rutas = new double[numeroCiudades][numeroCiudades];
        DecimalFormat redondea = new DecimalFormat("0.00");
        double valor;
        for (int i = 0; i < numeroCiudades; i++) {
            for (int j = 0; j < numeroCiudades; j++) {
                if (i != j) {
                    valor = (double) (Math.random()*(minimoValor-maximoValor)+maximoValor);
                    if (entero) {
                        rutas[i][j]=(int)Math.round(valor);
                    }else{
                        rutas[i][j]=valor;
                    }
                }
            }
        }
    }
    
    public void generarDesdeExcel(){
        LeerCaminos archivo = new LeerCaminos();
        rutas = archivo.getMatrizCaminos();
        numeroNodos=rutas.length;
    }
    
    public String imprimirRutas(){
        String cadena = "Rutas \n";
        for (int i = 0; i < numeroNodos; i++) {
            for (int j = 0; j < numeroNodos; j++) {
                cadena += rutas[i][j] + "\t";
            }
            cadena += "\n";
        }
        return cadena;
    }
}
