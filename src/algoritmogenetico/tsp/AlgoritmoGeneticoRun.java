package algoritmogenetico.tsp;

import algoritmogenetico.core.AlgoritmoGeneticoTSP;
import java.util.Scanner;

public class AlgoritmoGeneticoRun {

    public static void main(String[] args) {
        
        System.out.println("ALGORITMO GENETICO TSP \n");
        runMethod();
        
    }
    public static void runMethod(){
            AlgoritmoGeneticoTSP TSP = new AlgoritmoGeneticoTSP();
            System.out.println("OPCIONES DE EJCUCIÓN : ");
            System.out.println("1. Una sola corrida");
            System.out.println("2. Multiples corridas");
            System.out.println("3. Salir");
            System.out.print("Escoger opción : ");
            Scanner sc = new Scanner(System.in);
            int opcion = sc.nextInt();
            do {
                switch(opcion){
                    case 1:
                        System.out.println("\nUNA SOLA CORRIDA");
                        TSP.AGRunOne();
                        runMethod();
                        break;
                    case 2:
                        System.out.println("\nMULTIPLES CORRIDAS");
                        System.out.print("Número de veces en procesar la misma población inicial : ");
                        int n = sc.nextInt();
                        System.out.print("Número de poblaciones iniciales distintas : ");
                        int m = sc.nextInt();
                        System.out.println("\nNúmero total de repeticiones : " + (n*m));
                        TSP.AGRunMultiple( n , m );
                        runMethod();
                        break;
                    case 3:
                        System.exit(0);
                        break;
                }
            } while( opcion != 3);
        }

}
